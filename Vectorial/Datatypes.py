
class VectorialSpace:
    def __init__(self, _value):
        print " > Calling VectorialSpace class"
        self.value = _value

    def __add__(self, other):
        raise NotImplementedError("[ERROR] Not Implemented")

    def __rmul__(self, other):
        raise NotImplementedError("[ERROR] Not Implemented")


class Vector(VectorialSpace):

    def get_value(self):
        return self.value  # is a tuple that represent a R-n dimensional Vector

    def vector_sum_vector(self, other_vector):
        # if ( len(self.value) == len(other_vector.value)): raise #SomeError
        result = ()
        for i in range(0, len(other_vector.value)):
            sum = self.value[i] + other_vector.value[i]
            result = result + (sum,)
        return result

    def alpha_mult_vector(self, alpha):
        result = ()
        for i in range(0, len(self.value)):
            mult = alpha * self.value[i]
            result = result + (mult,)
        return result

    def __rmul__(self, other):  ## alpha*vector
        """
        Overloading * (multiply) operator (reverse form)
        Multiply alpha integer and Vector self.value
        Args:
            other: is a integer

        Returns: Vector result alpha*Vector
        """
        try:
            if type(other) == int:
                print "[OK] alpha_mult_vector"
                return self.alpha_mult_vector(other)
        except:
            print "[ERROR] not match"

    def __add__(self, other):
        """
        Overloading + (plus) operator
        Add other.value (Vector) and self.value (Vector)
        Args:
            other: Object Vector

        Returns: Vector resultant of self.value + other.value

        """
        try:
            if type(other.value) == tuple: ## Vector + Vector
                print "[OK] vector_sum_vector"
                result = self.vector_sum_vector(other)
                return result

        except:
            print "[ERROR] not match"


class Matrix(VectorialSpace):

    def get_value(self):
        return self.value # Is a list of list that represent a Matrix

    def matrix_sum_matrix(self, other_matrix):
        pass

    def alpha_mult_matrix(self, alpha):
        pass

    def __rmul__(self, other):  ##  alpha * Matrix
        # print type(other.value)
        try:
            if type(other) == int:
                self.alpha_mult_matrix(other)
                print "[OK] alpha_mult_matrix"

        except:
            print "[ERROR] not match"

    def __add__(self, other): ## Matrix + Matrix
        try:
            if type(other.value) == list:
                self.matrix_sum_matrix(other)
                print "[OK] matrix_sum_matrix"
        except:
            print "[ERROR] not match"


class Function(VectorialSpace):

    def get_value(self):
        return self.value  #is a str that represent a function

    def funct_sum_funct(self, other_matrix):
        pass

    def alpha_mult_funct(self, alpha):
        pass

    def __rmul__(self, other):  ## alpha*Function
        # print type(other.value)
        try:
            if type(other) == int:
                print "[OK] alpha_mult_funct"
                self.alpha_mult_funct(other)

        except:
            print "[ERROR] not match"

    def __add__(self, other):  ##
        try:
            if type(other.value) == str:  ## Function + Function
                print "[OK] funct_sum_funct"
                self.funct_sum_funct(other)

        except:
            print "[ERROR] not match"
